import React from 'react';

export const NavBar= () => {
    return (
        <React.Fragment>
            <nav className='navbar navbar-dark bg-dark navbar-expand-sm'>
                <div className="container">
                    <p className='navbar-brand'>
                         Product <span className='text-warning'>Search</span>
                    </p>
                </div>
            </nav>
        </React.Fragment>
    )
}
