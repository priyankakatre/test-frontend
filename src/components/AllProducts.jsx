import React, { useEffect, useState } from "react"
import { ProductService } from "../Services/ProductService.js";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from "react-bootstrap-table2-paginator";
import filterFactory , {textFilter, selectFilter} from "react-bootstrap-table2-filter";


export const AllProduct = () =>{
    let [state, setstate] = useState({
        loading: false,
        products: [],
        errorMessage: ''
    });

    const pagination = paginationFactory({
        page: 1,
        sizePerPage: 10,
        lastPageText: '>>',
        firstPageText:'<<',
        nextPageText: '>',
        prePageText: '<',
        showTotal: true,
        alwaysShowAllBtns: true,
      });
    useEffect( () => {
        async function fetchData() {
            try {
                setstate({...state, loading: true});
                let responce = await ProductService.getAllProducts();
                setstate({
                    ...state,
                    loading:false,
                    products: responce.data,
                });

            }
            catch(error) {
                setstate({
                    ...state,
                    loading:false,
                    errorMessage:error.message
                })
            }
        }
        fetchData();

    }, []);

    function imageFormatter(cell, row){
        return (<img style={{width:50}} src={cell}/>)
    }
    const selectOptions = {
        'dog': 'chewsdog',
        'chews': 'chews',
        'cat': 'cat'
    };

    const selectSubOptions = {
        'true': 'true',
        'false': 'false',
    };


    const columns = [{
        dataField: 'id',
        text: 'ID'
      }, {
        dataField: 'title',
        text: 'Title'
      }, {
        dataField: 'price',
        text: 'Price',
        filter: textFilter()
      },
      {
        dataField: 'subscription',
        text: 'Subscription',
        filter: selectFilter({
            options: selectSubOptions,
        })
      },
      {
        dataField: 'image_src',
        text: 'Image',
        formatter: imageFormatter
      },
      {
        dataField: 'tags',
        text: 'Tags',
        filter: textFilter()
}];

    return(
        <React.Fragment>
           <div className="product animated slideInLeft delay-.2s">
           <div className="container">
               <div className="row">
                   <div className="col">
                    <BootstrapTable
                            bootstrap5
                            keyField='id'
                            data={ state.products }
                            columns={ columns }
                            pagination ={pagination}
                            filter= {filterFactory()}>
                        </BootstrapTable>
                   </div>
               </div>
           </div>
           </div>
        </React.Fragment>
    )
}
