import './App.css';
import { NavBar } from './components/NavBar/NavBar';
import { AllProduct } from './components/AllProducts';

function App() {
  return (
    <div className="App">
         <NavBar/>
         <AllProduct/>
    </div>
  );
}

export default App;

