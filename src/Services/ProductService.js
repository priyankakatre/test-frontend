import axios from 'axios';

export class ProductService {
    static serviceUrl = `http://localhost:3010`;
    static getAllProducts() {
        let dataUrl = `${this.serviceUrl}/products`;
        return axios.get(dataUrl);
    }
}
